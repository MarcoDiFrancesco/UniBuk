import React, { Component } from 'react';

export default class Navbar extends Component {
  render() {
    return (
      <>
        <h1>About</h1>
        <p>This is going to be a web application we will develop as both project and exam for the "Software Engineering II" course.
           It is based on node.js as a backend server plus some more stuff I'm really not sure about.
           We need to develop an online service for e-commerce that will allow University students
            to sell, buy and search for used books, notes and resources. We want to deliver a web version of this service and a client application.</p>
      </>
    );
  }
}